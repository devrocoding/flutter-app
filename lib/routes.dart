// Utils and assets
import 'package:flutter/material.dart';

// Imported Routes
import 'screens/login/loginPage.dart';
import 'screens/splash.dart';

String HOME_SCREEN = "/home";

class Routes {

  Routes(){
    runApp(new MaterialApp(
      debugShowCheckedModeBanner: false,
      title: "GymProver",
      home: new SplashScreen(),
      routes: <String, WidgetBuilder>{
        HOME_SCREEN: (BuildContext context) => new LoginPage()
      },
    ));
  }
}