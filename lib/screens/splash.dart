import 'package:flutter/material.dart';
import 'package:flutter_app/routes.dart';
import 'dart:async';

class SplashScreen extends StatefulWidget{

  @override
  State createState() {
    return new SplashScreenState();
  }
}

class SplashScreenState extends State<SplashScreen>{

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    new Timer(
        new Duration(seconds: 1),
        () => Navigator.of(context).pushReplacementNamed(HOME_SCREEN)
    );
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      body: Stack(
        fit: StackFit.expand,
        children: <Widget>[
          new Container(
            decoration: new BoxDecoration(color: Colors.teal),
            child: new Image(
              image: new AssetImage("assets/logo2.png"),
            ),
          )
        ],
      ),
    );
  }
}