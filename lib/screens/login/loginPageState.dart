import 'package:flutter/material.dart';
import 'loginPage.dart';
import '../account/account.dart';
import 'package:http/http.dart' as http;
import 'package:toast/toast.dart';
import 'package:flutter_app/util/styling.dart';

class LoginPageState extends State<LoginPage> with SingleTickerProviderStateMixin{

  final scaffoldKey = new GlobalKey<ScaffoldState>();
  final formKey = new GlobalKey<FormState>();
  String _email;
  String _password;

  changeThePage(BuildContext context) {
    Navigator.of(context)
        .pushReplacement(MaterialPageRoute(
        fullscreenDialog: false,
        maintainState: true,
        builder: (context) => Account()
    )
    );
  }

  getData() async{
    http.Response response = await http.get(
        "https://www.school.devrocoding.com/api/index.php?username=jusjus112&password=112jusjus",
        headers: {
          "Accept": "application/json"
        }
    );

    print(response.body);
    return response.statusCode;
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  void _submit(){
    final form = formKey.currentState;

    switch(getData()){
      case 200:
        break;
    //Account matches
      case 404:
      //Account doesn't exists
        break;
      default:
      //Nothing filled in
        break;
    }

    if (form.validate()) {
      form.save();

      Toast.show("Invalid username or password!", context, duration: Toast.LENGTH_LONG, gravity:  Toast.BOTTOM);
    }
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
//      resizeToAvoidBottomPadding: false,
      body: new Stack(
        fit: StackFit.expand,
        children: <Widget>[
          new Image(
            image: new AssetImage("assets/background2.jpg"),
            fit: BoxFit.cover,
            color: Colors.black87,
            colorBlendMode: BlendMode.darken,
            width: 250,
          ),
          new Container(
            decoration: new BoxDecoration(color: Colors.white.withOpacity(0.6),backgroundBlendMode: BlendMode.overlay),
          ),
          Container(
            padding: const EdgeInsets.all(45.0),
            child: new Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                new Image(
                    image: new AssetImage("assets/firebase.png"),
                    height: 150,
                ),
//                new Image(
//                    image: new AssetImage("assets/logo_1.png")
//                ),
                Container(
                  padding: const EdgeInsets.only(top: 20.0),
                  child: new Form(
                    key: formKey,
                    child: new Theme(
                      data: new ThemeData(
                          brightness: Brightness.dark,
                          accentColor: Colors.teal,
                          inputDecorationTheme: new InputDecorationTheme(
                              labelStyle: new TextStyle(
                                  color: GymProverColor.MAIN,
                                  fontSize: 20
                              )
                          )
                      ),
                      child: new Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          new TextFormField(
                            decoration: new InputDecoration(
                                hintText: "Email or Username"
                            ),
                            validator: (val) => (
                                val.length>0 && !val.contains("@")
                                    ?'Username Or Emaill too short'
                                    :null
                            ),
                            keyboardType: TextInputType.emailAddress,
                            autovalidate: true,
                            autofocus: false,
//                            onEditingComplete: (val) => _email = val,
                          ),
                          new TextFormField(
                            decoration: new InputDecoration(
                                hintText: "Password"
                            ),
                            keyboardType: TextInputType.text,
                            validator: (val) => (
                                val.length>0 && val.length < 5
                                    ? 'Password doesn\'t meet the requirements'
                                    : null
                            ),
//                            onSaved: (val) => _password = val,
                            autovalidate: true,
                            autofocus: false,
                            obscureText: true,
                          ),
                          Container(
                            padding: const EdgeInsets.only(top: 20.0),
                            child: new MaterialButton(
                              child: new Text(
                                "GO BIG OR GO HOME",
                                style: new TextStyle(color: Colors.white),
                              ),
                              color: Colors.teal,
                              onPressed: _submit,
                              minWidth: MediaQuery.of(context).size.width * 0.65,
                              textColor: Colors.white,
//                              onPressed: ()=> changeThePage(context),
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                )
              ],
            ),
          )
        ],
      ),
    );
  }
}